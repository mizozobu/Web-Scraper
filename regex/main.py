import urllib.request
import re


url = 'https://www.lds.org/callings/missionary/mission-maps/all?lang=eng'
#put url here

req = urllib.request.Request(url)
resp = urllib.request.urlopen(req)
respData = resp.read()

paragraphs = re.findall(r'<p>(.*?)</p>', str(respData))

for eachP in paragraphs:
    print(eachP)
