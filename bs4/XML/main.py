import bs4 as bs


# XML scraper
def tag(tagname, content):
    opentag = '<%s>' % (tagname)
    closetag = '</%s>' % (tagname)
    htmltext = opentag + content + closetag
    return htmltext

# for file
xml = open('source.xml')
source = xml.read()

soup = bs.BeautifulSoup(source, 'xml')
file = open('scraped_XML.html', 'w')
locs = soup.find_all('loc')

for loc in locs:
    file.writelines(tag('p', loc.text))

file.close()
xml.close()