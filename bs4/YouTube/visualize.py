import bs4 as bs
import urllib.request
from datetime import datetime
import pandas as pd
import matplotlib.pyplot as plt


# YouTube channel scraper
def add_data(row):
    global df  # needed to modify global variable
    new_row = pd.Series(row, index=[
        'Title',
        'Today',
        'Published On',
        'Views',
        'Likes',
        'Dislikes'])
    df = df.append(new_row, ignore_index=True)


def get_data(link):
    source = urllib.request.urlopen(link).read()
    soup = bs.BeautifulSoup(source, 'lxml')

    title = soup.title.text.replace(' - YouTube', '')

    viewCountDiv = soup.find('div', class_='watch-view-count')
    viewCount = viewCountDiv.text.replace(',', '').replace(' views', '')
    viewCount = int(viewCount)

    datePublishedStrong = soup.find('strong', class_='watch-time-text')
    datePublished = datePublishedStrong.text.replace('Published on ', '')
    datePublished = datetime.strptime(datePublished, '%b %d, %Y')
    datePublished = datePublished.strftime('%m-%d-%Y')

    likeButton = soup.find('button', class_='like-button-renderer-like-button-unclicked')
    likeCount = likeButton.text.replace(',', '')
    likeCount = int(likeCount)

    dislikeButton = soup.find('button', class_='like-button-renderer-dislike-button-unclicked')
    dislikeCount = dislikeButton.text.replace(',', '')
    dislikeCount = int(dislikeCount)

    now = datetime.now()
    date = '%s-%s-%s' % (now.month, now.day, now.year)

    row = [title, date, datePublished, viewCount, likeCount, dislikeCount]

    add_data(row)


# Set DataFrame
youtubeData = {'Title': [],
               'Today': [],
               'Published On': [],
               'Views': [],
               'Likes': [],
               'Dislikes': []}

df = pd.DataFrame(youtubeData)

# YouTube Scraper
channelURL = 'https://www.youtube.com/user/TokaiOnAir/videos'
channelSource = urllib.request.urlopen(channelURL).read()
channelSoup = bs.BeautifulSoup(channelSource, 'lxml')

links = []
for url in channelSoup.find_all('a'):
    link = url.get('href')
    if '/watch' in link:
        link = 'https://www.youtube.com/' + link
        if link not in links:
            links.append(link)

for link in links:
    try:
        get_data(link)
    except Exception as e:
        print(link)

df = df.set_index(pd.DatetimeIndex(df['Published On']))
print(df)

plt.figure(1)
plt.subplot(211)
plt.plot(df['Views'])
plt.legend()

plt.subplot(212)
plt.plot(df['Likes'])
plt.plot(df['Dislikes'])
plt.legend()


plt.show()
