import bs4 as bs
import urllib.request
from datetime import datetime

# YouTube channel scraper
def GetData(link):
    source = urllib.request.urlopen(link).read()
    soup = bs.BeautifulSoup(source, 'lxml')

    title = soup.title.text.replace(' - YouTube', '')
    # print('Title: ' + title)

    viewCountDiv = soup.find('div', class_='watch-view-count')
    viewCount = viewCountDiv.text.replace(',', '').replace(' views', '')
    viewCount = int(viewCount)
    # print("Views: " + viewCount)

    datePublishedStrong = soup.find('strong', class_='watch-time-text')
    datePublished = datePublishedStrong.text.replace('Published on ', '')
    datePublished = datetime.strptime(datePublished, '%b %d, %Y')
    datePublished = datePublished.strftime('%m/%d/%Y')
    # print('Published on : ' + str(datePublished))

    likeButton = soup.find('button', class_='like-button-renderer-like-button-unclicked')
    likeCount = likeButton.text.replace(',', '')
    likeCount = int(likeCount)
    # print("Likes: " + likeCount)

    dislikeButton = soup.find('button', class_='like-button-renderer-dislike-button-unclicked')
    dislikeCount = dislikeButton.text.replace(',', '')
    dislikeCount = int(dislikeCount)
    # print("Dislikes: " + dislikeCount)

    now = datetime.now()
    date = '%s/%s/%s' % (now.month, now.day, now.year)

    row = [title, date, datePublished, viewCount, likeCount, dislikeCount]
    print(row)

# YouTube Scraper
channelURL = 'https://www.youtube.com/channel/UCpOjLndjOqMoffA-fr8cbKA/videos'
channelSource = urllib.request.urlopen(channelURL).read()
channelSoup = bs.BeautifulSoup(channelSource, 'lxml')

links = []
for url in channelSoup.find_all('a'):
    link = url.get('href')
    if '/watch' in link:
        link = 'https://www.youtube.com/' + link
        if link not in links:
            links.append(link)

for link in links:
    try:
        GetData(link)
    except Exception as e:
        print(link)